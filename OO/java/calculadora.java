public class Calculadora {
    private float resultado = 0;
    
    public Calculadora() {
        System.out.println("Calculadora criada!");
    }
    public float getResultado() {
        return this.resultado;
    }
    public void setResultado(float r) {
        this.resultado = r;
    }
    public float sum(float x, float y) {
        setResultado(x + y);
        return getResultado();
    }
    public float sub(float x, float y) {
        setResultado(x - y);
        return getResultado();
    }
    public float mult(float x, float y) {
        setResultado(x * y);
        return getResultado();
    }
    public float div(float x, float y) {
        setResultado(x / y);
        return getResultado();
    }
}
